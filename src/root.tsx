// @refresh reload
import { Suspense } from "solid-js";
import {
  A,
  Body,
  ErrorBoundary,
  FileRoutes,
  Head,
  Html,
  Meta,
  Routes,
  Scripts,
  Title,
} from "solid-start";
import { Typer } from "./components/Typer";
import "./root.css";

export default function Root() {
  return (
    <Html lang="en">
      <Head>
        <Title>Emma Fuller</Title>
        <Meta charset="utf-8" />
        <Meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta
          name="description"
          content="Personal website of Emma Fuller, software developer."
        />
      </Head>
      <Body>
        <Suspense>
          <ErrorBoundary>
            <main class="text-left mx-auto max-w-xl p-4 flex flex-col h-inner">
              <h1 class="text-3xl">Emma Fuller</h1>
              <hr class="border-2 border-teal-700 bg-teal-700 h-0 w-1/2" />
              <Typer
                messages={[
                  "full-stack developer",
                  "front-end developer",
                  "back-end developer",
                ]}
              />
              <Routes>
                <FileRoutes />
              </Routes>
              <div class="mb-5" />
              <div class="justify-self-end mt-auto flex justify-around">
                <A
                  end={true}
                  activeClass="border-b-4"
                  class="text-gray-500 font-semibold hover:border-b-4 border-cyan-700"
                  href="/"
                >
                  Home
                </A>
                <A
                  end={true}
                  activeClass="border-b-4"
                  class="text-gray-500 font-semibold hover:border-b-4 border-cyan-700"
                  href="/projects"
                >
                  Projects
                </A>
              </div>
            </main>
          </ErrorBoundary>
        </Suspense>
        <Scripts />
      </Body>
    </Html>
  );
}
