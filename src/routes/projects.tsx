import { For } from "solid-js";
import { createRouteData, useRouteData } from "solid-start";
import { fetchGql, gql } from "../utils/gql";

const repoDataQuery = gql`
  query GetProjects($ids: [ID!]) {
    projects(ids: $ids) {
      nodes {
        id
        webUrl
        name
        description
        languages {
          name
          share
        }
      }
    }
  }
`;

type Project = {
  id: string;
  name: string;
  description: string;
  webUrl: string;
  languages: { name: string; share: number }[];
};

export const routeData = () =>
  createRouteData<Project[]>(async () => {
    const ids = [
      `gid://gitlab/Project/41990434`, // EmmaFuller.dev
      `gid://gitlab/Project/40958941`, // js-match
      `gid://gitlab/Project/42219668`, // AoC2022
    ];

    return (
      await fetchGql("https://gitlab.com/api/graphql", repoDataQuery, { ids })
    ).data.projects.nodes.sort((a: Project, b: Project) => ids.indexOf(a.id) - ids.indexOf(b.id));
  });

export default function Home() {
  const projects = useRouteData<typeof routeData>();

  return (
    <div class="mt-3 flex flex-col">
      <For each={projects()}>
        {(data) => (
          <div>
            <div class="flex flex-col justify-between mt-5 sm:flex-row">
              <a class="text-tag w-auto self-start" href={data.webUrl}>
                {data.name}
              </a>
              <div class="text-gray-500 font-semibold">{data.description}</div>
            </div>
            <div class="flex justify-around text-xs text-gray-500 mt-1 font-light">
              <For each={data.languages}>
                {(language) => (
                  <span>
                    {language.name} - {language.share}%
                  </span>
                )}
              </For>
            </div>
          </div>
        )}
      </For>
    </div>
  );
}
