import { formatDistance } from "date-fns";

export default function Home() {
  return (
    <div class="mt-3">
      <div class="mt-2">
        Hey I'm <strong class="text-teal-700">Emma Fuller</strong>. I write code
        for a living 💻
      </div>

      <div class="mt-2">
        I was born somewhere in Virginia, but I am currently based in{" "}
        <strong class="text-teal-700">Fort Collins, Colorado</strong>. I work at{" "}
        <a href="https://indeed.com" class="text-tag">
          Indeed
        </a>{" "}
        where I am helping to build out the UI platform. I've been working in
        the software industry for{" "}
        <strong class="text-teal-700">
          {formatDistance(new Date(), new Date(2017, 5))}
        </strong>
        , and have dabbled in a large range of web technologies, from JQuery and
        AngularJS to Solid and Svelte, and most everything in between.
      </div>

      <div class="mt-2">
        You can find me on{" "}
        <a
          class="text-tag"
          href="https://www.linkedin.com/in/emma-fuller-664b88122/"
        >
          LinkedIn
        </a>{" "}
        and{" "}
        <a class="text-tag" href="https://gitlab.com/EmmaMaeFuller">
          GitLab
        </a>
        , or contact me directly via{" "}
        <a class="text-tag" href="mailto:emma@emmafuller.dev">
          email
        </a>
      </div>

      <div class="mt-2">
        I am <strong class="text-teal-700">not</strong> currently seeing
        employment opportunities.
      </div>

      <div class="mt-2 text-gray-500 font-semibold">
        Ⓒ {new Date().getFullYear()} Emma Fuller
      </div>
    </div>
  );
}
