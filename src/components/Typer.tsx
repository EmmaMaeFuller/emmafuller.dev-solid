import { createSignal, mergeProps } from "solid-js";
import { Show } from "solid-js/web";

import { delay, delayBefore, delayAfter } from "~/utils/async";
import { interval, loop } from "~/utils/lifecycle";
import { WithDefaults } from "~/utils/typing";

const defaultProps = {
  prompt: "$ ",
  cursor: "▎",
  charMillis: 200,
  lineMillis: 1000,
  cursorMillis: 500,
};

type TyperProps = WithDefaults<{ messages: string[] }, typeof defaultProps>;

export function Typer(props: TyperProps) {
  const p = mergeProps(defaultProps, props);

  const cursor = interval((c) => !c, { initial: false, delay: p.cursorMillis });
  const [text, setText] = createSignal(props.messages[0]);

  const type = async (full: string, part: string = "") => {
    await delayBefore(() => setText(part), p.charMillis);
    await (full !== part
      ? type(full, full.slice(0, part.length + 1))
      : delay(p.lineMillis));
    await delayAfter(() => setText(part), p.charMillis);
  };

  loop(
    async (index: number) => {
      await type(p.messages[index]);
      return (index + 1) % p.messages.length;
    },
    { initial: 0, delay: p.lineMillis }
  );

  return (
    <div aria-hidden="true">
      {p.prompt}
      {text()}
      <Show when={cursor()}>{p.cursor}</Show>
    </div>
  );
}
