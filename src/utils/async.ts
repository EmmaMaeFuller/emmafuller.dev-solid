export const delay = (millis: number) =>
  new Promise((resolve) => setTimeout(resolve, millis));

export const delayBefore = async (fn: () => void, millis: number) => {
  await delay(millis);
  await fn();
};

export const delayAfter = async (fn: () => void, millis: number) => {
  await fn();
  await delay(millis);
};
