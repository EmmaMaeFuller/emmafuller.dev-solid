import { createSignal, onCleanup } from "solid-js";

export function loop<Data>(
  fn: (arg0: Data) => Data | Promise<Data>,
  options: { initial: Data; delay: number }
) {
  let payload = options.initial;
  let running = true;

  (async () => {
    while (running) {
      payload = await fn(payload);
      await new Promise((resolve) => setTimeout(resolve, options.delay));
    }
  })();

  onCleanup(() => (running = false));
}

export function interval<Data>(
  fn: (arg0: Data) => Data,
  options: { initial: Data; delay: number }
) {
  const [sig, setSig] = createSignal(options.initial);

  loop(async () => setSig(fn), options);

  return sig;
}
