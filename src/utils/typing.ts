export type WithDefaults<Mandatory extends object, Optional extends object> = {
    [Prop in keyof (Mandatory & Partial<Optional>)]: (Mandatory & Partial<Optional>)[Prop];
}