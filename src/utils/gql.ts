// This workaround allows for graphql syntax highlighting in VSCode without needing to parse as a graphql object
export const gql = String.raw;

export const fetchGql = (
  url: string,
  query: string,
  variables?: { [key: string]: any }
) => {
  return fetch(url, {
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query, variables }),
    method: "POST",
  }).then((r) => r.json());
};
