import { mount, StartClient } from "solid-start/entry-client";

const setInnerHeight = () =>
  document.documentElement.style.setProperty(
    "--doc-height",
    `${window.innerHeight}px`
  );

window.addEventListener("resize", setInnerHeight);
setInnerHeight();

mount(() => <StartClient />, document);
